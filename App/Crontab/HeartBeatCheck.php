<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-11-6
 * Time: 下午4:28
 */

namespace App\Crontab;


use App\TcpController\MachineMsgController;
use EasySwoole\EasySwoole\Crontab\AbstractCronTask;
use EasySwoole\EasySwoole\Crontab\Crontab;

class HeartBeatCheck extends AbstractCronTask
{

    public static function getRule(): string
    {
        // TODO: Implement getRule() method.
        // 定时周期 （每分钟一次）
        return '*/1 * * * *';
    }

    public static function getTaskName(): string
    {
        // TODO: Implement getTaskName() method.
        // 定时任务名称
        return "HeartBeatCheck";
    }

    static function run(\swoole_server $server,int $taskId,int $fromWorkerId,$flags = null)
    {
        // TODO: Implement run() method.
        // 定时任务处理逻辑

        $MachineMsgController = new MachineMsgController();
        $MachineMsgController->checkLastHearBeatTime();
        echo "cron HeartBeatCheck run at ".date("Y-m-d H:i:s").PHP_EOL;
        // 可以获得当前任务的规则 任务下一次执行的时间 任务总计执行的次数
//        $cron = Crontab::getInstance();
//        $current = date('Y-m-d H:i:s');
//        $rule = $cron->getTaskCurrentRule('HeartBeatCheck');
//        $nextTime = date('Y-m-d H:i:s',$cron->getTaskNextRunTime('HeartBeatCheck'));
//        $runCount = $cron->getTaskRunNumberOfTimes('HeartBeatCheck');
//
//        var_dump("cron HeartBeatCheck run at {$current} currentRule: {$rule} next: {$nextTime} count: {$runCount}");
    }
}