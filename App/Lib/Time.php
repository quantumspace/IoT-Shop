<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 2019/4/11
 * Time: 下午4:33
 */

namespace App\Lib;


class Time extends \EasySwoole\Utility\Time
{
    public static function format_dbtimestamp($time='')
    {
        $time = empty($time) ? time() : $time;
        return date('YmdHis',$time);
    }

}