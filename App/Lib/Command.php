<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 2019/4/6
 * Time: 上午10:52
 */

namespace App\Lib;


use EasySwoole\Component\Singleton;
use EasySwoole\Socket\Bean\Caller;

class Command
{
    use Singleton;
    #1.包头
    #开始标识(2字节) 0xEF3A
    #长度(2字节) (信道、协议版本、命令、内容、CRC 五者的字节之 和(高字节在前))
    #信道(1字节) 0x01 发送包
    #    0x11  应答包
    #协议版本(1字节) 0x01,0x02...0xFF

#2.包体
    #命令(1字节)
    #内容 N字节

#3.包尾
    #CRC校验(2字节) 是指将开始标识、长度、信道、协议版本、命令和内容这六 部分内容的所有字节进行 CRC 计算(高字节在前)。

#ASCII码转换

    #包头开始标识
    const START_FLAG = 0xEF3A;

    #协议版本
    const VERSION = 0x01;

    #域名或ip
    public $host = '106.15.226.8';
    public $port = '9603';

    #信道
    const channel = [
        'request'   => 0x01,  //表示发送包
        'response'  => 0x11,  //表示应答包
    ];

    protected $header = [2,2,1,1];//单位字节

    //body形式
    //request一方永远都是客户端
    //response一方永远都是服务端(包括服务器主动请求)
    protected $body = [
        0x01 => [
            'request'  => [1,6,4],//单位字节
            'response' => [1,6,200,2,4,1],//单位字节
        ],
        0x02 => [
            'request'  => [1,6,2,4],//单位字节
            'response' => [1,6,4,1],//单位字节
        ],

        #读取智能售货机的一条uid信息
        0x03 => [
            'request'  => [1,6,8,4,1],//单位字节
            'response' => [1,6,4],//单位字节
        ],

        #写一条uid信息到智能售货机
        0x04 => [
            'request'  => [1,6,8,4,1],//单位字节
            'response' => [1,6,8,4],//单位字节
        ],

        #删一条uid信息到智能售货机
        0x05 => [
            'request'  => [1,6,8,4,1],//单位字节
            'response' => [1,6,8,4],//单位字节
        ],

        #开门指令(远程操作)
        0x06 => [
            'request'  => [1,6,8,1,4,1],//单位字节
            'response' => [1,6,8,1,4],//单位字节
        ],
        #售货机上报开门信息(现场操作)
        0x07 => [
            'request'  => [1,6,8,1,4],//单位字节
            'response' => [1,6,8,1,4,1],//单位字节
        ],
        #心跳包
        0x08 => [
            'request'  => [1,6,1,4],//单位字节
            'response' => [1,6,4,4,1],//单位字节
        ],
        #清空uid所有指令
        0x09 => [
            'request'  => [1,6,4,1],//单位字节
            'response' => [1,6,4],//单位字节
        ],
        0x0A => [
            'request'  => [1,6,4],//单位字节
            'response' => [1,6,200,2,4,1],//单位字节
        ],
        0x0B => [
            'request'  => [1,6,4],//单位字节
            'response' => [1,6,4,4,1],//单位字节
        ],
        0x0C => [
            'request'  => [1,6,4],//单位字节
            'response' => [1,6,200,2,4,1],//单位字节
        ],
        0x0E => [
            'request'  => [1,6,4,1],//单位字节
            'response' => [1,6,4],//单位字节
        ],
    ];

    protected $footer = [2];

    private $used_request_rule;
    private $returned_rule; //返回数据运用的规则临时存储处

    public $errors ;

    private $command_list_arr = [];//command容器,由$this->body中的key组成

    protected $request_buff = [];

    protected $request_hexstr = '';
    protected $request_hexbuff_arr;

    #结果码
    const SUCC_CODE = 0x01;
    const ERR_CODE  = 0x02;
    const PACK_ERR = 0x03;
    const LATEST_VERSION = 0x04;

    protected  $buff = [];

    public $debug = false;

    static function getInstance(...$args)
    {
        if(!isset(self::$instance)){
            self::$instance = new static(...$args);
        }
        self::$instance -> buff = []; //每次执行都清空
        return self::$instance;
    }

    public function __construct($request_hexStr='')
    {
        $this -> command_list_arr = array_keys($this->body);
        $this -> setRequestStr($request_hexStr);
    }



    /**
     * 检查设备发送过来的消息是否是有效的消息
     */
    public function check_request_isValid()
    {
        $headers_hexStr_arr = $this -> split_hexstr_byrules($this->request_hexstr,$this->header);

        //包头检查是否包含self::START_FLAG标识
        if(empty($headers_hexStr_arr) || empty($headers_hexStr_arr[0]) || hexdec($headers_hexStr_arr[0]) != self::START_FLAG){
            $this -> errors = '开始不包含'.sprintf('%04X',self::START_FLAG);
            return false;
        }

        $header_command_hexStr_arr = $this -> split_hexstr_byrules($this->request_hexstr,array_merge($this->header,[1]));

        //检查是否有命令字段
        if(empty($header_command_hexStr_arr[4])){
            $this -> errors = '消息中不包含命令字段';
            return false;
        }

        #检查命令是否在合法的命令列表
        if(!in_array(hexdec($header_command_hexStr_arr[4]),$this->command_list_arr)){
            $this -> errors = $header_command_hexStr_arr[4].'不在命令列表('.implode(',',$this->command_list_arr).')';
            return false;
        }

        $request_rules = array_merge(
            $this->header,
            $this->body[hexdec($header_command_hexStr_arr[4])]['request'],
            $this->footer
        );
        $request_hexstr_all = $this -> split_hexstr_byrules($this->request_hexstr,$request_rules);

//        var_dump($request_hexstr_all);
        $this -> request_hexbuff_arr = $request_hexstr_all;

        #crc字段不存在
        if(empty($request_hexstr_all[count($request_hexstr_all)-1])){
            $this -> errors = 'crc字段不存在或缺少!';
            return false;
        }

        #循环冗余校验相应的命令是否满足要求
        $request_crc_value = array_pop($request_hexstr_all);
        $crc_value = $this -> MyCrc16Check($request_hexstr_all);
        if($crc_value == $request_crc_value){
            return true;
        }
        $this -> errors = '消息中crc字段值'.$request_crc_value.'与计算后的'.$crc_value.'不符';
        return false;
    }


    public function getResBuff()
    {
        $res = $this -> check_request_isValid();
        if(!$res){
            return false;
        }
        return $this -> request_hexbuff_arr;
    }


    /**
     * 设置发送发送过来的16进制请求字符串
     * @param string $request_hexStr
     * @return $this
     */
    public function setRequestStr(string $request_hexStr)
    {
        if($request_hexStr instanceof Caller){
            $this -> request_hexstr = $request_hexStr -> getArgs()['raw'];
        }else{
            $this -> request_hexstr = $request_hexStr;
        }
        return $this;
    }


    /**
     * 智能售货机从中心服务器获取具体要链接的前置服务器域名或 IP
     * 方向：服务器（响应） -> 智能售货机
     */
    public function response_01()
    {
        #解析发送过来的数据并拆分
        $this -> get_request_buff($this->request_hexstr,0x01);

        #设置包头
        $this -> set_header();

        #命令
        $this -> buff[] = sprintf('%02X',0x01);

        #智能售货机mac码
        $this -> buff[] = $this -> getMacFromRequesst(); //已转16进制字符串

        #域名或ip为ascII码
        $this -> buff[] = $this -> string2AscII($this -> host); //已转16进制字符串

        #端口号
        $this -> buff[] = sprintf('%04X',$this -> port);

        #时间戳
        $this -> buff[] = $this -> getTimeStampFromRequest(); //已转16进制字符串

        #结果码(暂时设置为成功，主要用于占位)
        $this -> buff[] = sprintf('%02X',self::SUCC_CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x01,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     * 智能售货机向服务器申报 MAC 码指令
     * 方向: 服务器（响应） -> 智能售货机
     * @param int $CODE
     * @return bool|string
     */
    public function response_02($CODE=self::SUCC_CODE)
    {
        $CODE = empty($CODE) ? self::SUCC_CODE : $CODE;

        #解析发送过来的数据并拆分，此时可能需要循环冗余校验
        $this -> get_request_buff($this->request_hexstr,0x02);

        #设置包头
        $this -> set_header();

        #命令
        $this -> buff[] = sprintf('%02X',0x02);

        #智能售货机mac码
        $this -> buff[] = $this -> getMacFromRequesst(); //已转16进制字符串

        #时间戳
        $this -> buff[] = $this -> getTimeStampFromRequest(7); //已转16进制字符串

        #结果码(暂时设置为成功，主要用于占位)
        $this -> buff[] = sprintf('%02X',$CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x02,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *从智能售货机读取一条 UID 指令
     *方向 服务器（主动请求） -> 智能售货机
     * @param $mac string  智能售货机mac码(12位的16进制字符串)
     * @return void
     */
    public function response_03($mac)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x03);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac); //已转16进制字符串

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x03,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *从智能售货机写一条 UID 指令
     *方向 服务器（主动请求） -> 智能售货机
     * @param $mac string  智能售货机mac码(12位的16进制字符串)
     * @param $uid string  Ic卡的uid码，16位的16进制字符串
     * @return void
     */
    public function response_04($mac,$uid)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x04);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #ic卡的uid
        $this -> buff[] = $this -> setUid($uid);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x04,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *从智能售货机删除一条 UID 指令
     *方向 服务器（主动请求） -> 智能售货机
     * @param $mac string  智能售货机mac码(12位的16进制字符串)
     * @param $uid string  Ic卡的uid码，16位的16进制字符串
     * @return void
     */
    public function response_05($mac,$uid)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x05);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #ic卡的uid
        $this -> buff[] = $this -> setUid($uid);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x05,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *远程开门指令
     *方向 服务器（主动请求） -> 智能售货机
     * @param $mac string  智能售货机mac码(12位的16进制字符串)
     * @param $uid string  Ic卡的uid码，16位的16进制字符串
     * @return void
     */
    public function response_06($mac,$uid)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x06);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #ic卡的uid
        $this -> buff[] = $this -> setUid($uid);

        #开门方式
        $this -> buff[] = sprintf('%02X',0x01);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x06,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *智能售货机开门信息上报到服务器
     *方向 服务器（响应） -> 智能售货机
     * @param int $CODE
     * @return bool|string
     */
    public function response_07($CODE=self::SUCC_CODE)
    {
        #解析发送过来的数据并拆分，此时可能需要循环冗余校验
        $this -> get_request_buff($this->request_hexstr,0x07);

        #设置包头
        $this -> set_header();

        #命令
        $this -> buff[] = sprintf('%02X',0x07);

        #智能售货机mac码
        $this -> buff[] = $this -> getMacFromRequesst(); //已转16进制字符串

        #uid
        $this -> buff[] = $this -> getUidFromRequest();

        #开门方式
        $this -> buff[] = sprintf('%02X',0x01);

        #时间戳
        $this -> buff[] = $this -> getTimeStampFromRequest(8); //已转16进制字符串

        #结果码(暂时设置为成功，主要用于占位)
        $this -> buff[] = sprintf('%02X',$CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x07,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     *心跳包
     *方向 服务器（响应） -> 智能售货机
     * @param int $CODE
     * @return void
     */
    public function response_08($CODE=self::SUCC_CODE)
    {
        #解析发送过来的数据并拆分，此时可能需要循环冗余校验
        $this -> get_request_buff($this->request_hexstr,0x08);

        #设置包头
        $this -> set_header();

        #命令
        $this -> buff[] = sprintf('%02X',0x08);

        #智能售货机mac码
        $this -> buff[] = $this -> getMacFromRequesst(); //已转16进制字符串

        #时间戳
        $this -> buff[] = $this -> getTimeStampFromRequest(7); //已转16进制字符串

        #校准时间
        $this -> buff[] = $this -> setTimeStamp(time());

        #结果码(暂时设置为成功，主要用于占位)
        $this -> buff[] = sprintf('%02X',$CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x08,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     * 清空智能售货机所有uid
     * 方向 服务器（请求） -> 智能售货机
     */
    public function response_09($mac)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x09);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x09,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     * 服务器向智能售货机发送远程重启指令
     * 方向 服务器（请求） -> 智能售货机
     */
    public function response_0E($mac)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x0E);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x0E,'response'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     * 智能售货机向服务器请求ip地址和端口
     * 上报mac码
     */
    public function request_01($mac)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x01);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x01,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**
     * 智能售货机客户端请求
     * 上报mac码
     */
    public function request_02($mac)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x02);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #版本号
        $this -> buff[] = sprintf('%04X',0x04);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x02,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }


    /**读uid
     * 智能售货机返回ui给服务器
     *
     */
    public function request_03($mac,$uid)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x03);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #智能售货机uid
        $this -> buff[] = $this -> setUid($uid);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #结果码
        $this -> buff[] = sprintf('%02X',self::SUCC_CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x03,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**写uid
     * 智能售货机返回结果给服务器
     * @param $mac
     * @param $uid
     * @return bool|string
     */
    public function request_04($mac,$uid)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x04);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #智能售货机uid
        $this -> buff[] = $this -> setUid($uid);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #结果码
        $this -> buff[] = sprintf('%02X',self::SUCC_CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x04,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**删uid
     * 智能售货机返回结果给服务器
     * @param $mac
     * @param $uid
     * @return bool|string
     */
    public function request_05($mac,$uid)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x05);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #智能售货机uid
        $this -> buff[] = $this -> setUid($uid);;

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #结果码
        $this -> buff[] = sprintf('%02X',self::SUCC_CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x05,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }


    /**开门
     * 售货机应答给服务器
     * @param $mac
     * @param $uid
     * @return bool|string
     */
    public function request_06(string $mac,string $uid)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x06);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);;

        #智能售货机uid
        $this -> buff[] = $this -> setUid($uid);;

        #开门方式
        $this -> buff[] = sprintf('%02X',0x01);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #结果码
        $this -> buff[] = sprintf('%02X',self::SUCC_CODE);

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x06,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**开门信息上报到服务器
     * 售货机请求服务器
     * @param $mac
     * @param $uid
     * @param $open_type 开门方式 0x01 ic卡  0x02 APP远程2G
     * @return bool|string
     */
    public function request_07($mac,$uid,$open_type)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x07);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);;

        #智能售货机uid
        $this -> buff[] = $this -> setUid($uid);

        #开门方式
        $this -> buff[] = sprintf('%02X',$open_type);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x07,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /*售货机发送心跳包给服务器
     * 售货机请求服务器
     * @param $mac
     * @param $uid
     * @param $door_status 门状态 0x01 开门  0x02 关门
     */
    public function request_08($mac,$door_status)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x08);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #们状态
        $this -> buff[] = sprintf('%02X',$door_status);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x08,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /* 清除uid信息
      * 售货机响应服务器
      * @param $mac
      */
    public function request_09($mac,$CODE=self::SUCC_CODE)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x09);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #结果码
        $this -> buff[] = sprintf('%02X',$CODE);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x09,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**报警信息上传到服务器
     * 售货机请求服务器
     * @param $mac
     * @param $warning_type
     * @return void
     */
    public function request_0B($mac,$warning_type)
    {
        #设置包头
        $this -> set_header('request');

        #命令
        $this -> buff[] = sprintf('%02X',0x0B);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);;

        #报警类型
        $this -> buff[] = sprintf('%02X',$warning_type);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x0B,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    /**重启
     * 售货机响应服务器
     * @param $mac
     * @return void
     */
    public function request_0E($mac)
    {
        #设置包头
        $this -> set_header('response');

        #命令
        $this -> buff[] = sprintf('%02X',0x0E);

        #智能售货机mac码
        $this -> buff[] = $this -> setMac($mac);

        #时间戳
        $this -> buff[] = $this -> setTimeStamp(time()); //已转16进制字符串

        #获取长度并赋值
        $this -> buff[1] = sprintf('%04X',$this -> get_length(0x0E,'request'));

        #设置包尾
        $this -> set_footer();

        return $this -> format_response($this->buff);
    }

    public function setTimeStamp($time)
    {
        return sprintf('%08X',$time);
    }

    /**获取智能售货机mac码(前提是通过构造方法传入16进制的请求字符串，或者调用setRequestStr传入了16进制字符串)
     * @return bool|mixed
     */
    public function getMacFromRequesst($index = 5)
    {
        if(!empty($this->request_buff)){
            return $this -> request_buff[$index];
        }
        return false;
    }

    public function getTimeStampFromRequest($index = 6)
    {
        if(!empty($this->request_buff)){
            return $this -> request_buff[$index];
        }
        return false;
    }

    public function getUidFromRequest($index = 6)
    {
        if(!empty($this->request_buff)){
            return $this -> request_buff[$index];
        }
        return false;
    }



    /**根据command指令来获取规则
     * @param $hex_str
     * @param $command
     */
    public function get_request_buff($hex_str,$command)
    {
        $request_buff_rule = array_merge($this -> header,$this -> body[$command]['request'],$this -> footer);
        $this -> request_buff = $this -> split_hexstr_byrules($hex_str,$request_buff_rule);
        $this -> used_request_rule = $request_buff_rule;
    }

    /**CRC校验
     * @param $str    待校验的CRC字节组成
     * @return int
     */
    public function MyCrc16Check($str)
    {
        if(is_array($str)){
            $str = implode('',$str);
        }

        #将16进制的str切分成2个16进制为一组的字节数组
        $char_byte = [];
        $index = -1;
        for($i = 0 ; $i < strlen($str); $i++){
            if($i % 2 == 0){
                $index++;
                $char_byte[$index] = $str[$i];
            }else{
                $char_byte[$index].= $str[$i];
            }
        }

        $DIV_VALUE = 0x4821;
        $A_VALUE = 0xFFFF;
        $Crc_Value = $A_VALUE;
//        var_dump(count($char_byte).'字节');
        for($i = 0 ; $i < count($char_byte) ; $i++){
            $Crc_Value = $Crc_Value ^ hexdec($char_byte[$i]);
            for($j = 0 ; $j < 8 ; $j++){
                $My_Check = $Crc_Value & 1;
                $Crc_Value = $Crc_Value >> 1;
                if($My_Check == 1){
                    $Crc_Value = $Crc_Value ^ $DIV_VALUE;
                }
            }
        }
        return sprintf('%04X',$Crc_Value);
    }

    //包头设置
    private function set_header($type_of_return='response')
    {
        #开始标志
        $this->buff[0] = sprintf('%04X',self::START_FLAG);

        #长度 占位Null
        $this->buff[1] = null;

        #信道
        $this->buff[2] = sprintf('%02X',self::channel[$type_of_return]);

        #协议版本
        $this->buff[3] = sprintf('%02X',self::VERSION);
    }

    /**
     * 设置包尾
     */
    private function set_footer()
    {
        $this -> buff[] = $this -> MyCrc16Check($this->buff);
    }


    private function setMac($mac)
    {
        return $mac;
//        return sprintf('%012X',$mac);
    }

    private function setUid($uid)
    {
        return $uid;
//        return sprintf('%016X',$uid);
    }

    /**根据规则来分割16进制字符串
     * @param $str    要分割的16进制字符串
     * @param $rules  规则数组 [2,2,1,1,1,6,2,4,2]; //单位为字节
     * @return array  返回根据规则字节长度分割之后的数组
     */
    private function split_hexstr_byrules($str,$rules)
    {
        $index = 0;
        foreach($rules as $key => $bytenum){
            $arr[$key] = substr($str,$index,$bytenum * 2);
            $index += $bytenum * 2 ;
        }
        return $arr;
    }

    /**
     * 计算指令的总长度
     *
     */
    private function get_length($command,$type,$start = 2)
    {
        $buff_rule = array_merge($this->header,$this->body[$command][$type],$this->footer);
        $bytenum_sum = 0;
        foreach($buff_rule as $k => $bytenum){
            if($k < $start){
                continue;
            }
            $bytenum_sum += $bytenum;
        }
        return $bytenum_sum;
    }

    //字符串转ASCII码（16进制表示，200个字节 多余的用ox00填充）
    private function string2AscII($str)
    {
        $ASCII = '';
        for ( $pos=0; $pos < strlen($str); $pos ++ ) {
            $byte = substr($str, $pos);
            $ASCII.= sprintf('%02X',ord($byte));
        }
        $ASCII = sprintf('%0400s',$ASCII);
        return $ASCII;
    }

    private function format_response($buff)
    {
        if($this->debug){
            echo '<pre>';
            echo '请求原始数据:<br/>';
            echo $this -> request_hexstr;
            echo '<br/>';
            echo "<br/>";

            echo '请求数据处理规则';
            print_r($this->used_request_rule);
            echo '<br/>';

            echo '请求数据运用如上规则处理后:<br/>';
            print_r($this -> request_buff);
            echo '<br/>';

            echo '返回的数据格式<br/>';
            print_r($buff);
            echo '</pre>';

            echo '最终形成的发送数据格式是:';
            echo implode('',$buff);
            echo str_repeat('*',1000).'<br/>';
            return true;
        }
//        return hex2bin(implode('',$buff));
        return implode('',$buff);
    }






}