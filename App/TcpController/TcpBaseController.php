<?php


namespace App\TcpController;


use App\Model\Tcp\MachineConnectionsModel;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\EasySwoole\Swoole\Task\TaskManager;
use EasySwoole\Socket\AbstractInterface\Controller;

class TcpBaseController extends Controller
{
    public function getParams()
    {
       return $this->caller()->getArgs();
    }

    /**
     * 获取元素命令字符串
     * @return string
     */
    public function getRawCommand():string
    {
        $data =  $this->getParams()['raw']??'';
        return $data;
    }

    /**
     * 获取格式化后的命令数组
     * @return array
     */
    public function getBody()
    {
        $data =  $this->getParams()['body']??[];
        return $data;
    }

    /**
     * 获取swoole客户端
     * @return mixed
     */
    public function getClient()
    {
        $client = $this->caller()->getClient();
        return $client;
    }

    /**
     * 获取当前连接的句柄
     * @return mixed
     */
    public function getFd()
    {
        $client = $this->getClient();
        $fd     = $client->getFd();
        return $fd;
    }


    /**
     * 断开连接
     * @param int $fd
     * @return mixed
     */
    public function close(int $fd = 0)
    {
        $fd  = empty($fd)?$this->getFd():$fd;
        return ServerManager::getInstance()->getSwooleServer()->close($fd);
    }


    /**
     * 发送消息
     * @param string $command
     * @param int $fd
     */
    public function send(string $command, int $fd = 0)
    {
        if ($fd == 0){
            $this->response()->setMessage($command);
        }else{
            var_dump('发送的命令是');
            var_dump($command);
            var_dump('fd是'.$fd);
            ServerManager::getInstance()->getSwooleServer()->send($fd,$command);
        }
    }


    public function send_msg_by_mac(string $command, string $mac = '')
    {
        //如果为空则默认向当前连接发送消息
        $mac_model =  new MachineConnectionsModel();
        $fd = empty($mac) ? $this -> getFd() : $mac_model -> getFDByMac($mac);
        if(empty($fd)){
            echo "mac 不存在";
            return false;
        }
        return $this->send($command,$fd);
    }


}