<?php


namespace App\TcpController;


use App\Model\Tcp\MachineConnectionsModel;
use EasySwoole\Socket\AbstractInterface\Controller;
use swoole_server;


class TcpEventController extends Controller
{
    public static function onConnect(swoole_server $server,int $fd,int $reactor_id)
    {
        echo "tcp服务  fd:{$fd} 已连接\n";
    }

    public static function onClose(swoole_server $server,int $fd,int $reactor_id)
    {
        $model = new  MachineConnectionsModel();
        $model->closeConnection($fd);
        echo "tcp服务  fd:{$fd} 已关闭\n";
    }

}