<?php


namespace App\TcpController;


use App\Lib\Command;
use App\Model\Machine\MachineModel;
use App\Model\Machine\MacModel;
use App\Model\Machine\UidModel;
use App\Model\Order\OrderModel;
use App\Model\Tcp\MachineConnectionsModel;
use EasySwoole\EasySwoole\Trigger;
use EasySwoole\Utility\Str;
use PhpParser\Node\Scalar\String_;

class MachineMsgController extends TcpBaseController
{
    function actionNotFound(?string $actionName)
    {
        $this->response()->setMessage("{$actionName} not found \n");
    }

    /**发送过来的消息包错误无法解析的回调事件
     * @return bool
     */
    function package_error()
    {
        $res =  Command::getInstance()->request_02('0A0B0C123456');
//        var_dump($res);
//        $this -> func_write_uid('0A0B0C123456','1234567891234567');
        return $this -> send_msg_by_mac(Command::getInstance()->setRequestStr($this->caller())->response_08(Command::PACK_ERR));
    }

    /**
     * 从中心服务器获取具体要链接的前置服务器域名或 IP
     */
    public function func_01()
    {
//        Command::getInstance()->debug = true;
//        Command::getInstance()->request_08('0A0B0C123456','1234567891234567',1);
//        $this -> func_write_uid('0A0B0C123456','1234567891234567');
        return $this -> send_msg_by_mac(Command::getInstance()->setRequestStr($this->getRawCommand())->response_01());
    }

    /**
     *mac上报命令方法
     * @return bool
     */
    public function func_02()
    {
        $fd     = $this->getFd();
        if (empty($fd)){
            return false;
        }
        $body   = $this->getBody();
        $mac    = $body[0]??'';
        if (empty($mac)){
            return false;
        }
        $model  = new MachineConnectionsModel();
        $model->initConnection($fd,$mac);

        #ToDo 上报的mac需要存储到数据库以便后台可以进行选择绑定。
        $mac_model = new MacModel();
        if(!$mac_model -> save_mac($mac)){
            $code = Command::ERR_CODE;
        }
        $command = Command::getInstance()->setRequestStr($this->getRawCommand())->response_02($code??'');
        return $this -> send_msg_by_mac($command,$mac);
    }

    /**服务器接收客户端的响应
     * (从智能售货机读取一条uid)
     */
    public function func_03()
    {
        $body = $this -> getBody();
        $uid = $body[1];
        #Todo 待定 暂时不需要处理
    }

    /**
     * 服务器接收客户端的响应
     * (向智能售货机写一条uid)
     */
    public function func_04()
    {
        $body = $this -> getBody();
        $uid = $body[1];
        $mac = $body[0];
        $uid_model = new UidModel();
        if(!$uid_model -> save($mac,$uid)){
            Trigger::getInstance()->error($uid_model->error);
            return false;
        }
        #Todo 写入成功，通知websocket端client?
    }

    /**
     * 服务器接收客户端的响应
     * (向智能售货机删除一条uid)
     */
    public function func_05()
    {
        $body = $this -> getBody();
        $uid = $body[1];
        $mac = $body[0];
        $uidModel = new UidModel();
        if(!$uidModel -> delete($mac,$uid)){
            Trigger::getInstance()->error($uidModel->error);
            return false;
        }
        #Todo 删除成功，通知websocket端client?
    }

    /**服务器接收客户端的响应
     * 响应服务器的远程开门的处理逻辑
     */
    public function func_06()
    {
        $body = $this -> getBody();
        $uid = $body[1];
        $mac = $body[0];
        $open_type = $body[2];
        #Todo 远程开门后的处理，订单状态更新?

    }

    /**服务器接收客户端的响应
     * 开门信息上报
     */
    public function func_07()
    {
        $body           = $this -> getBody();
        $uid            = $body[1];
        $mac            = $body[0];
        $open_type      = $body[2];
        #Todo 如果开门方式是刷IC卡开门，库存和商品状态更新，货柜商品状态更新
        if ($open_type  ==0x02){
            $orderModel = new OrderModel();
            $flag       = $orderModel->modifyOrderStatusByMac($mac);
        }
        var_dump('开门方式是'.$open_type);
    }

    /**
     * 服务器回应客户端的心跳包
     */
    public function func_08()
    {
        $body = $this->getBody();
        //TODO: 获取 mac
        $mac  = $body[0];
        $time = dechex($body[1]);
        $model= new MachineConnectionsModel();
        $flag = $model->updateHeartbeatByMac($mac,$time);
        return $this -> send_msg_by_mac(Command::getInstance()->setRequestStr($this->getRawCommand())->response_08());
    }


    /**
     * 心跳检测 每分钟扫描一次
     * @return bool
     */
    public function checkLastHearBeatTime()
    {
        $model      = new MachineConnectionsModel();
        $macList    = $model->getMachineOnlineMacList();
        if (empty($macList)){
            return false;
        }
        foreach ($macList as $mac){
            //TODO: 根据 mac 和 上次 心跳时间
            $last_time = $model->getLastHeartbeatTimeByMac($mac);
            //TODO: 当前时间 - 上次 心跳时间 大于5分钟 断开mac对应的fd连接
            if (time()-$last_time>300){
                echo "{$mac}触发心跳超时".PHP_EOL;
                $fd = $model->getFDByMac($mac);
                $this->close($fd);
                return false;
            }
            return true;
        }
    }


    /**服务器响应客户端清空uid的消息
     */
    public function func_09()
    {
        $body = $this -> getBody();
        $mac = $body[0];
        $uidModel = new UidModel();
        if(!$uidModel -> clear($mac)){
            Trigger::getInstance()->error($uidModel->error);
            return false;
        }
        #Todo 删除成功，通知websocket端client
    }

    /**
     * 服务器响应客户端远程重启指令
     */
    public function func_0E()
    {
        $body = $this -> getBody();
        $mac = $body[0];

    }


    /**
     * 从智能售货机读取一条uid
     * @param String $mac
     * @return bool|void
     */
    public function func_get_uid(String $mac)
    {
        return $this -> send_msg_by_mac(Command::getInstance()->response_03($mac),$mac);
    }

    /**向智能售货机写一条uid
     * @param String $mac
     * @param String $uid
     * @return bool
     */
    public function func_write_uid(String $mac , String $uid)
    {
        return $this -> send_msg_by_mac(Command::getInstance()->response_04($mac,$uid),$mac);
    }

    /**
     * 向智能售货机删一条uid
     * @param String $mac
     * @param String $uid
     * @return bool|void
     */
    public function func_del_uid(String $mac,String $uid)
    {
        return $this -> send_msg_by_mac(Command::getInstance()->response_05($mac,$uid),$mac);
    }


    /**
     * 清空智能售货机所有Uid
     * @param String $mac
     * @return bool|void
     */
    public function func_clear_all_uid(String $mac)
    {
        return $this -> send_msg_by_mac(Command::getInstance()->response_09($mac),$mac);
    }

    /**
     * 远程重启指令
     */
    public function func_restart(String $mac)
    {
        return $this -> send_msg_by_mac(Command::getInstance()->response_0E($mac),$mac);
    }



    /**
     * 指定mac 发送开门命令
     * @param string $mac
     * @return bool|void
     */
    public function func_open(string $mac)
    {
        $machineModel        = new MachineModel();
        $machine_uid         = $machineModel->getMachineUidByMac($mac);

        $machineConnectModel = new MachineConnectionsModel();
        $fd                  = $machineConnectModel->getFDByMac($mac);
        if (empty($fd)||empty($machine_uid)){
            Trigger::getInstance()->error('machine_uid或fd为空,fd:'.$fd.',machine_uid'.$machine_uid);
            return false;
        }
        $command             = new Command();
        $open_com            = $command->response_06($mac,$machine_uid);
        $flag                = $this->send_msg_by_mac($open_com,$mac);
        return $flag;
    }





}