<?php
/**
 * Created by PhpStorm.
 * User: Tioncico
 * Date: 2018/10/17 0017
 * Time: 9:10
 */
namespace App\TcpController;

use App\Lib\Command;
use EasySwoole\EasySwoole\Trigger;
use EasySwoole\Socket\Bean\Caller;
use EasySwoole\Socket\Bean\Response;
use EasySwoole\Socket\AbstractInterface\ParserInterface;
use EasySwoole\Utility\CommandLine;

class Parser implements ParserInterface
{
    public function decode($raw, $client): ?Caller
    {
        // TODO: Implement decode() method.
        $Command = new Command($raw);
        $bean   = new Caller();
        $request_buff_arr = $Command -> getResBuff();
        if(!$request_buff_arr){
            //无效的售货机消息
            Trigger::getInstance()->error('无效的售货机消息:'.$Command->errors);
            $bean->setControllerClass(MachineMsgController::class);
            $bean->setAction('package_error');
            $data   = ['raw'=>$raw,'array'=>$request_buff_arr];
            $bean->setArgs($data);
            return $bean;
        }

        $body   = array_slice($request_buff_arr,5,count($request_buff_arr)-5-1);
        $action = "func_".$request_buff_arr[4];
        $data   = ['raw'=>$raw,'array'=>$request_buff_arr,'body'=>$body];
        $bean->setControllerClass(MachineMsgController::class);
        $bean->setAction($action);
        $bean->setArgs($data);
        return $bean;
    }

    public function encode(Response $response,$client): ?string
    {
        return $response;
    }


}