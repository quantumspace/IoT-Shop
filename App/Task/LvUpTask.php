<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/18
 * Time: 14:22
 * Auth: YWH
 */

namespace App\Task;
use App\Model\Ip\IpModel;
use App\Model\Relation\RelationModel;
use App\Model\User\UserBaseModel;
use EasySwoole\EasySwoole\Swoole\Task\AbstractAsyncTask;

class LvUpTask extends AbstractAsyncTask
{
    public function run($taskData, $taskId, $fromWorkerId,$flags = null)
    {
        // TODO: Implement run() method.
        if ($taskData['type']=='ip'){
            $model        = new IpModel();
            $result       = $model->checkIpLvUp($taskData['ip_id']);
        }elseif ($taskData['type'] == 'fans'){
            $model        = new RelationModel();
            $result       = $model->checkFansLvUp($taskData['uid']);
        }elseif ($taskData['type'] == 'user'){
            $model        = new UserBaseModel();
            $result       = $model->checkUserLvUp($taskData['uid']);
        }else{
            $result       = [];
        }

        return ['result'=>$result,'taskData'=>$taskData];
//        echo "执行task模板任务\n";
    }

    function finish($result, $task_id)
    {
        return $result;
        // TODO: Implement finish() method.
    }


}