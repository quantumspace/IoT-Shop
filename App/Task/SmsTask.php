<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 2018/10/24 0024
 * Time: 15:58
 */

namespace App\Task;


use App\Model\User\SmsModel;
use App\Serve\smsServer;
use EasySwoole\EasySwoole\Swoole\Task\AbstractAsyncTask;


class SmsTask extends AbstractAsyncTask
{
    function run($taskData, $taskId, $fromWorkerId,$flags = null)
    {
        // TODO: Implement run() method.
        if (!is_array($taskData)||empty($taskData['login_name'])||empty($taskData['code'])){
           echo "数据错误";
            return false;
        }
        $result       = smsServer::sendSms($taskData['login_name'],['code'=>$taskData['code']]);
//        echo "执行task模板任务\n";
        return ['result'=>$result,'taskData'=>$taskData];
    }

    function finish($result, $task_id)
    {
        $smsDemo = new  SmsModel();
        $smsDemo->smsCodePersistence($result);
        return $result;
        // TODO: Implement finish() method.
    }


}