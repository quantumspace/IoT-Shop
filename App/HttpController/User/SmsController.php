<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/10
 * Time: 10:26
 * Auth: YWH
 */

namespace App\HttpController\User;


use App\BaseController\ApiController;
use App\Model\User\SmsModel;
use App\Model\User\UserBaseModel;

class SmsController extends ApiController
{
    public function sendSMSWhenUserRegister()
    {
        $req    = $this->getJsonParams();
        if (empty($req['login_name'])){
            $this->writeJsonWithResponse([],-1,'用户名不能为空');
        }
        $userBaseModel = new UserBaseModel();
        $userBaseInfo  = $userBaseModel->getUserBaseInfo($req['login_name']);
        if (!empty($userBaseInfo)){
            return $this->writeJsonWithResponse([],-1,'手机号已被占用');
        }
        $code   = $this->getSMSCode();
        $result = $this->sendSMSTask($req['login_name'],$code);
        return $this->writeJsonWithResponse(['code'=>$code],200);
    }

    public function sendSMSWhenUserForgetPassword()
    {
        $req    = $this->getJsonParams();
        if (empty($req['login_name'])){
            $this->writeJsonWithResponse([],-1,'用户名不能为空');
        }
        $userBaseModel = new UserBaseModel();
        $userBaseInfo  = $userBaseModel->getUserBaseInfo($req['login_name']);
        if (empty($userBaseInfo)){
            return $this->writeJsonWithResponse([],-1,'账号不存在，是否前往注册？');
        }
        $code   = $this->getSMSCode();
        $result = $this->sendSMSTask($req['login_name'],$code);
        return $this->writeJsonWithResponse(['code'=>$code],200);
    }


    public function sendSMSCode()
    {
        $req    = $this->getJsonParams();
        if (empty($req['login_name'])){
            $this->writeJsonWithResponse([],-1,'用户名不能为空');
        }
        $code   = $this->getSMSCode();
        $result = $this->sendSMSTask($req['login_name'],$code);
        return $this->writeJsonWithResponse(['code'=>$code],200);
    }



}