<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/15
 * Time: 14:50
 * Auth: YWH
 */

namespace App\HttpController\System;


use App\BaseController\ApiController;
use App\Model\System\SystemConfModel;

class SystemController extends ApiController
{
    public function getSystemConf()
    {
        $systemModel = new SystemConfModel();
        $data        = $systemModel->getSystemConfWithApp();
        return $this->writeJsonWithResponse($data);
    }
}