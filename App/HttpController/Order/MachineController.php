<?php


namespace App\HttpController\Order;


use App\BaseController\ApiController;
use App\TcpController\MachineMsgController;

class MachineController extends ApiController
{
    /**开门
     * @return string
     * @throws \ErrorException
     */
    public function OpenDoor()
    {
        $mac =  $this -> getJsonParams('mac');
        if(empty($mac)){
            return $this->writeJsonWithResponse(null,400,'mac地址不能为空');
        }
        $MachineController = new MachineMsgController();
        $res = $MachineController -> func_open($mac);
        return $this->writeJsonWithResponse([],200,'开门消息发送成功');
    }

    /**向智能售货机写一条uid
     * @return string
     * @throws \ErrorException
     */
    public function WriteUid()
    {
        $mac =  $this -> getJsonParams('mac');
        $uid =  $this -> getJsonParams('uid');

        if(empty($mac)){
            return $this->writeJsonWithResponse(null,400,'mac地址不能为空');
        }

        if(empty($uid)){
            return $this->writeJsonWithResponse(null,400,'uid不能为空');
        }

        $MachineController = new MachineMsgController();
        $res = $MachineController -> func_write_uid($mac,$uid);
        return $this->writeJsonWithResponse([],200,'写uid信息发送成功');
    }

    /**删除售货机指定Uid
     * @return string
     * @throws \ErrorException
     */
    public function DeleteUid()
    {
        $mac =  $this -> getJsonParams('mac');
        $uid =  $this -> getJsonParams('uid');

        if(empty($mac)){
            return $this->writeJsonWithResponse(null,400,'mac地址不能为空');
        }

        if(empty($uid)){
            return $this->writeJsonWithResponse(null,400,'uid不能为空');
        }

        $MachineController = new MachineMsgController();
        $res = $MachineController -> func_del_uid($mac,$uid);
        return $this->writeJsonWithResponse([],200,'删除uid信息发送成功');
    }

    /**清空售货机Uid
     * @return string
     * @throws \ErrorException
     */
    public function ClearUid()
    {
        $mac =  $this -> getJsonParams('mac');

        if(empty($mac)){
            return $this->writeJsonWithResponse(null,400,'mac地址不能为空');
        }

        $MachineController = new MachineMsgController();
        $res = $MachineController -> func_clear_all_uid($mac);
        return $this->writeJsonWithResponse([],200,'清空uid信息发送成功');
    }

    /**远程重启指令
     * @return string
     * @throws \ErrorException
     */
    public function Restart()
    {
        $mac =  $this -> getJsonParams('mac');
        if(empty($mac)){
            return $this->writeJsonWithResponse(null,400,'mac不能为空');
        }
        $MachineController = new MachineMsgController();
        $res = $MachineController -> func_restart($mac);
        return $this->writeJsonWithResponse([],200,'重启指令发送成功');
    }


}