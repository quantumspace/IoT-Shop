<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/4
 * Time: 17:09
 * Auth: YWH
 */

namespace App\BaseController;


class ResponseController
{
    /**
     * @param null $data
     * @param string $ret
     * @param string $msg
     * @return array
     * @throws \ErrorException
     */
    public static function response($data = null, int $ret=200, $msg='操作成功'):array
    {
        if (!empty($data)&&!is_object($data)&&!is_array($data)){
            throw new  \ErrorException('data类型错误');
        }

        if (empty($data)) {
            if (is_object ($data)){
                $res =  ['ret'=>intval($ret),'msg'=>$msg,'data'=>new \stdClass()];
            }else{
                $res =  ['ret'=>intval($ret),'msg'=>$msg,'data'=>[]];
            }
        }else{
            $res =  ['ret'=>intval($ret),'msg'=>$msg,'data'=>$data];
        }
        if (!empty($res['data']) && is_array($res['data'])) {
            foreach ($res['data'] as  &$val) {
                if (is_numeric ( $val )) {
                    $val = strval($val);
                }elseif (is_bool ($val)){
                    $val = strval((int)$val);
                } elseif (is_Null($val)){
                    $val = "";
                } elseif (is_array ($val)){
                    $val = self::changeString($val);
                }
            }
        }

        return $res;
    }

    public static function changeString($array):array
    {
        foreach ($array as  $key=>$val) {
            if (is_numeric ( $val )||is_bool ($val)) {
                $array[$key] = strval($val);
            }elseif (is_Null($val)){
                $array[$key] = "";
            }elseif(is_array($val)){
                $array[$key] = self::changeString($val);
            }else if(is_object($val)){

            }
        }
        return $array;
    }

}