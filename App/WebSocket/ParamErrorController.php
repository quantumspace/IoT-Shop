<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/12
 * Time: 11:31
 * Auth: YWH
 */

namespace App\WebSocket;


class ParamErrorController extends BaseWebSocketController
{
    public function index()
    {
        $this->send([],-201,"class map error");
    }
    public function token()
    {
        $this->send([],-202,"Missing parameters token");
    }

    public function ParamEmpty()
    {
        $this->send([],-203,"param empty error");
    }

    public function loginErr()
    {
        $this->send([],-95,"登录异常");
    }
}