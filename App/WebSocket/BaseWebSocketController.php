<?php
/**
 * Created by PhpStorm.
 * User: YWH
 * Date: 2019/1/19
 * Time: 16:28
 */
namespace App\WebSocket;

use App\BaseController\ResponseController;
use App\Model\User\WebsocketFdModel;
use EasySwoole\EasySwoole\ServerManager;
use EasySwoole\EasySwoole\Swoole\Task\TaskManager;
use EasySwoole\Socket\AbstractInterface\Controller;


class BaseWebSocketController extends Controller
{
    public function getJsonParam()
    {
        $req   = $this->caller()->getArgs();
        return $req;
    }

    public function pushMsgToUserByUid($uid,$data)
    {
        $model  = new WebsocketFdModel();
        $fd     = $model->getFdByUid($uid);
        $data   = is_array($data)?json_encode($data):$data;
        TaskManager::async(function () use ($fd,$data){
            $server = ServerManager::getInstance()->getSwooleServer();
                $server->push($fd,$data);
        });
    }

    public function send($data,$ret=200,string $msg = '操作成功')
    {
        if (!is_array($data)&&!is_object($data)){
            throw  new \Error(" Uncaught TypeError : data mast be object or array");
        }
        $data                         = ResponseController::response($data,$ret,$msg);
        $debug                        = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        $dubug                        = array_pop($debug);
        $data['event']['controller']  = ltrim($dubug['class'],"App\\WebSocket\\");
        $data['event']['action']      = $dubug['function'];
        krsort($data);
        $data                         = is_array($data)||is_object($data)?json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES):$data;
        return $this->response()->setMessage($data);
    }
}