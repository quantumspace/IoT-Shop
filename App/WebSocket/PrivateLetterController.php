<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/2/12
 * Time: 10:01
 * Auth: YWH
 */

namespace App\WebSocket;


use App\Model\Ip\MessageLogModel;
use App\Model\Ip\MessageModel;
use App\Utility\Pool\MysqlObject;
use App\Utility\Pool\MysqlPool;
use EasySwoole\Component\Pool\Exception\PoolEmpty;
use EasySwoole\Component\Pool\Exception\PoolUnRegister;

class PrivateLetterController extends BaseWebSocketController
{
    public function index()
    {
        $this->response()->setMessage("action参数错误");
    }

    public function getLimitMsgByIpId()
    {
        $req          = $this->caller()->getArgs();
        $messageModel = new  MessageModel();
        if (empty($req['ip_id'])){
            return $this->send([],-1,'缺少ip_id');
        }
        $data         = $messageModel->getLimitMsgByIpId($req['ip_id']);
        $data         = empty($data)?new  \stdClass():$data;
        return $this->send($data);
    }

    public function sendPrivateLetter()
    {
        $req          = $this->caller()->getArgs();
        if (empty($req['limit_msg_id'])||!is_numeric($req['limit_msg_id'])){
            return $this->send([],-1,'缺少limit_msg_id 或者 limit_msg_id 错误');
        }

        $messageModel = new  MessageModel();
        $data         = $messageModel->getLimitMsgByIpLimitId($req['limit_msg_id']);
        if (empty($data)){
            return $this->send([],-2,'不支持的粉丝福利消息');
        }
        try {
            MysqlPool::invoke(function (MysqlObject $mysqlObject) use ($req,$data) {
                $msgLogModel  = new MessageLogModel();
                #用户发送记录
                $fromLog      = [
                    "msg_content" => $data['limited_msg'],
                    "receive_id"  => $data["ip_uid"],
                    "send_id"     => $req['uid']
                ];
                $flag1         = $msgLogModel->addMsgLog($fromLog,$mysqlObject);
                #ip自动回复记录
                $toLog        = [
                    "msg_content" => $data['reply_msg'],
                    "receive_id"  => $req["uid"],
                    "send_id"     => $data['ip_uid']
                ];
                $flag2         = $msgLogModel->addMsgLog($toLog,$mysqlObject);
                if ($flag1>0&&$flag2>0){
                    $mysqlObject->commit();
                }else{
                    $mysqlObject->rollback();
                }
            });
        } catch (\Throwable $throwable) {
           $this->send([],-301,'DB ERR');
        }catch (PoolEmpty $poolEmpty){
            $this->send([],-301,'DB ERR');
        }catch (PoolUnRegister $poolUnRegister){
            $this->send([],-301,'DB ERR');
        }
        $data         = empty($data)?new \stdClass():$data;
        return $this->send($data);
    }


    public function getMsgListFromIp()
    {
        $req          = $this->getJsonParam();
        if (empty($req['receive_id'])||empty($req['send_id'])){
            return $this->send([],-1,"param 'receive_id' or 'send_id' missing");
        }
        $msgLogModel  = new MessageLogModel();
        $data         = $msgLogModel->getMsgListFromIp($req['receive_id'],$req['send_id']);
        return $this->send($data);
    }
}