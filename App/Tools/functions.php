<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/18
 * Time: 10:35
 * Auth: YWH
 */
namespace App\Tools\func;
function getDayBeginAndEnd()
{
    $beginToday =   mktime(0,0,0,date('m'),date('d'),date('Y'));
    $endToday   =   mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
    return ['beginToday'=>$beginToday,'endToday'=>$endToday];
}