<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/15
 * Time: 10:59
 * Auth: YWH
 */

namespace App\Model\System;


use App\Model\BaseAutoModel;
use App\Serve\CacheKeyController;

class SystemConfModel extends BaseAutoModel
{
    protected $table = 'cmc_system_conf';
    public function getSystemConf ()
    {
        $keyInfo    = CacheKeyController::$SYS_CONF;
        $data       = $this->redis->get($keyInfo['key']);
        $data       = json_decode($data,true);
        if (empty($data)){
            $conf       = $this->db->get($this->table);
            $data       = [];
            foreach ($conf as $key=>$value){
                $data[$value['conf_name']] = $value['conf_val'];
            }
            $this->redis->setEx($keyInfo['key'],$keyInfo['time'],json_encode($data));
        }
        return $data;
    }

    public function getSystemConfWithApp()
    {
        $conf       = $this
            ->db
            ->where("conf_type",1,'=')
            ->get($this->table);
        if (empty($conf)){
            return new \stdClass();
        }
        $data       = [];
        foreach ($conf as $key=>$value){
            $data[$value['conf_name']] = $value['conf_val'];
        }
        return $data;
    }
}