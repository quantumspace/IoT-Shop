<?php
namespace  App\Model\Tcp;
use App\Model\BaseAutoModel;
use App\Model\Tcp\CacheKeyNameForTcpModel;



class MachineConnectionsModel extends BaseAutoModel
{
    /**
     * 机器连接初始化处理
     * @param int $fd
     * @param string $mac
     * @return bool
     */
    public function initConnection(int $fd,string $mac)
    {

        $this->AssociatingFDWithMAC($fd,$mac);
        $this->setMachineOnline($fd,$mac);
        return true;
    }


    /**
     * 机器断开连接处理
     * @param int $fd
     * @return bool
     */
    public function closeConnection(int $fd)
    {
        $mac  = $this->getMacByFd($fd);
        if (empty($mac)){
            return false;
        }
        $this->setMachineOffline($fd,$mac);
    }

    /**
     * mac fd  相互映射
     * @param int $fd
     * @param string $mac
     */
    public function AssociatingFDWithMAC(int $fd, string $mac)
    {
        $macToFdKey = CacheKeyNameForTcpModel::getMacByFdKeyName();
        $FdToMacKey = CacheKeyNameForTcpModel::getFDByMACKeyName();
        $this->redis->multi();
        $this->redis->hSet($macToFdKey,$mac,$fd);
        $this->redis->hSet($FdToMacKey,$fd,$mac);
        $this->redis->exec();
    }


    /**
     * 机器上线 redis数据更新
     * @param int $fd
     * @param string $mac
     */
    public function setMachineOnline(int $fd, string $mac)
    {
        $machineOnlineFdListKey  = CacheKeyNameForTcpModel::getOnlineMachineFdListKeyName();
        $machineOnlineMacListKey = CacheKeyNameForTcpModel::getOnlineMachineMacListKeyName();
        $this->redis->sAdd($machineOnlineFdListKey,$fd);
        $this->redis->sAdd($machineOnlineMacListKey,$mac);
    }


    /**
     * 获取在线mac 列表
     * @return mixed
     */
    public function getMachineOnlineMacList()
    {
        $machineOnlineMacListKey = CacheKeyNameForTcpModel::getOnlineMachineMacListKeyName();
        $macList                 = $this->redis->sMembers($machineOnlineMacListKey);
        return $macList;
    }


    /**
     * 获取在线fd列表
     * @return mixed
     */
    public function getMachineOnlineFdList()
    {
        $machineOnlineFdListKey  = CacheKeyNameForTcpModel::getOnlineMachineFdListKeyName();
        $FDList                  = $this->redis->sMembers($machineOnlineFdListKey);
        return $FDList;
    }


    /**
     * 机器下线 redis数据更新
     * @param int $fd
     * @param string $mac
     */
    public function setMachineOffline(int $fd, string $mac)
    {
        $machineOnlineFdListKey  = CacheKeyNameForTcpModel::getOnlineMachineFdListKeyName();
        $machineOnlineMacListKey = CacheKeyNameForTcpModel::getOnlineMachineMacListKeyName();
        $this->redis->srem($machineOnlineFdListKey,$fd);
        $this->redis->srem($machineOnlineMacListKey,$mac);
    }


    /**
     * 根据fd查询mac
     * @param string $mac
     * @return mixed
     */
    public function getFDByMac(string $mac):?int
    {
        $macToFdKeyName = CacheKeyNameForTcpModel::getMacByFdKeyName();
        $fd            = $this->redis->hGet($macToFdKeyName,$mac);
        return $fd;
    }


    /**
     * 根据mac查询fd
     * @param int $fd
     * @return string|null
     */
    public function getMacByFd(int $fd):?string
    {
        $macToFdKeyName = CacheKeyNameForTcpModel::getFDByMACKeyName();
        $mac            = $this->redis->hGet($macToFdKeyName,$fd);
        return $mac;
    }


    /**
     * 更新上次心跳时间
     * @param string $mac
     * @param int $timestamp
     * @return mixed
     */
    public function updateHeartbeatByMac(string $mac, int $timestamp)
    {
        $hb_keyName = CacheKeyNameForTcpModel::getHeartbeatKeyByMac($mac);
        $flag       = $this->redis->setEx($hb_keyName,300,$timestamp);
        return $flag;
    }

    /**
     * 获取上次心跳时间
     * @param $mac
     * @return mixed
     */
    public function getLastHeartbeatTimeByMac($mac)
    {
        $hb_keyName      = CacheKeyNameForTcpModel::getHeartbeatKeyByMac($mac);
        $timestamp       = $this->redis->get($hb_keyName);
        return $timestamp;
    }







}