<?php
namespace App\Model\Tcp;

class CacheKeyNameForTcpModel
{
    public static function getFDByMACKeyName():string
    {
        return "Hash|mac-fd";
    }

    public static function getMacByFdKeyName():string
    {
        return "Hash|fd-mac";
    }

    public static function getOnlineMachineFdListKeyName():string
    {
        return "Set|machine_online_fd_list";
    }

    public static function getOnlineMachineMacListKeyName():string
    {
        return "Set|machine_online_mac_list";
    }


    public static function getHeartbeatKeyByMac(string $mac):string
    {
        return "String|Heartbeat|mac:{$mac}";
    }



}