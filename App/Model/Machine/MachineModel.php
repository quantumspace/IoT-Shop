<?php


namespace App\Model\Machine;


use App\Model\BaseAutoModel;
use EasySwoole\EasySwoole\Trigger;

class MachineModel extends BaseAutoModel
{
    protected $table = "wx_machine";
    public function getMachineUidByMac(string $mac):string
    {
        $data = $this->db
            ->join("wx_machine_uid","wx_machine_uid.machine_id = wx_machine.machine_id")
            ->where("wx_machine.mac",$mac)
            ->getOne($this->table,'wx_machine_uid.machine_uid');
        $machine_uid = $data["machine_uid"]??'';
        return $machine_uid;
    }

    public function getMachineDataByMac($mac)
    {
        try{
            return $this -> db
                -> where('mac',$mac)
                -> getOne($this -> table);
        }catch(\Throwable $throwable){
            Trigger::getInstance()->error($throwable->getMessage());
            return false;
        }
    }
}