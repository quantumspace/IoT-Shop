<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 2019/4/11
 * Time: 下午4:40
 */

namespace App\Model\Machine;


use App\Lib\Time;
use App\Model\BaseAutoModel;

class UidModel extends BaseAutoModel
{
    protected $table = 'wx_machine_uid';
    public $error = '';

    public function save($mac,$uid)
    {
        $machine_data = (new MachineModel())->getMachineDataByMac($mac);
        if(empty($machine_data)){
            $this -> error = '该mac号:'.$mac.'在wx_machine表中不存在!';
            return false;
        }
        $is_exists_uid = $this -> db
            -> where('machine_uid',$uid)
            -> where('machine_id',$machine_data['machine_id'])
            -> getOne($this->table);

        #已经写入过了
        if(!empty($is_exists_uid)){
            return true;
        }

        #新增
        $res = $this -> db
            -> insert($this -> table,[
                'machine_id' => $machine_data['machine_id'],
                'machine_uid' => $uid,
                'update_time' => Time::format_dbtimestamp(),
                'create_time' => Time::format_dbtimestamp()
            ]);
        if(empty($res)){
            $this -> error = '插入失败';
            return false;
        }
        return true;
    }

    public function read()
    {

    }

    public function delete($mac,$uid)
    {
        $machine_data = (new MachineModel())->getMachineDataByMac($mac);
        if(empty($machine_data)){
            $this -> error = '该mac号:'.$mac.'在wx_machine表中不存在!';
            return false;
        }

        $res = $this -> db
            -> where('machine_id',$machine_data['machine_id'])
            -> where('machine_uid',$uid)
            -> delete($this->table);

        if(empty($res)){
            $this -> error = '删除失败';
            return false;
        }
        return true;
    }

    /**清空该设备的uid
     * @param $mac
     * @return bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function clear($mac)
    {
        $machine_data = (new MachineModel())->getMachineDataByMac($mac);
        if(empty($machine_data)){
            $this -> error = '该mac号:'.$mac.'在wx_machine表中不存在!';
            return false;
        }
        $res = $this -> db
            -> where('machine_id',$machine_data['machine_id'])
            -> delete($this->table);
        if(empty($res)){
            $this -> error = '删除失败';
            return false;
        }
        return true;
    }
}