<?php
/**
 * Created by PhpStorm.
 * User: zhou
 * Date: 2019/4/11
 * Time: 下午3:17
 */

namespace App\Model\Machine;


use App\Lib\Time;
use App\Model\BaseAutoModel;
use EasySwoole\EasySwoole\Trigger;

class MacModel extends BaseAutoModel
{
    protected $table = 'wx_mac';

    public function save_mac($mac)
    {
        try{
            $data = $this -> db
                -> where('mac',$mac)
                -> getOne($this->table,'mac');

            if(!empty($data)){
                #该Mac已经上报过
                return true;
            }

            #未上报的Mac存储进数据库
            if($this -> db -> insert($this->table,[
                'mac'        => $mac,
                'created_at' => Time::format_dbtimestamp(),
                'updated_at' => Time::format_dbtimestamp(),
            ])){
                return true;
            }

            #插入失败
            return false;

        }catch (\Throwable $throwabl){
            #sql执行失败
            Trigger::getInstance()->error($throwabl->getMessage());
            return false;
        }
    }
}