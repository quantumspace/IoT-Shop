<?php
/**
 * 带redis和mysql 的 模型基类
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/9
 * Time: 9:15
 * Auth: YWH
 */

namespace App\Model;

use App\BaseController\ResponseController;
use App\Utility\Pool\{MysqlObject,RedisObject,MysqlPool,RedisPool};
use EasySwoole\Component\Pool\PoolManager;
use EasySwoole\EasySwoole\Config;


class BaseAutoModel
{
    /**@var $db MysqlObject**/
    public $db       = null;
    /**@var $redis RedisObject**/
    public $redis    = null;

    public function __construct()
    {
            //**********mysql start********
            $mysql = $this->initMysql();
            $this->db =$mysql;
            //**********mysql end********

            //**********redis start********
            $redis       = $this->initRedis();
            $this->redis = $redis;
            //**********redis end********
    }


    protected function getDbConnection(): MysqlObject
    {
        return $this->db;
    }

    public function initMysql():MysqlObject
    {
        $timeout        = Config::getInstance()->getConf('MYSQL.POOL_TIME_OUT');
        $mysqlObject    = PoolManager::getInstance()->getPool(MysqlPool::class)->getObj($timeout);

        // 请注意判断类型 避免拿到非期望的对象产生误操作
        if ($mysqlObject instanceof MysqlObject) {
            $this->db = $mysqlObject;
        } else {
            //直接抛给异常处理，不往下
            throw new \Exception('error,Mysql Pool is Empty');
        }
        return $mysqlObject;
    }


    /**
     * @return
     * @throws \Exception
     */
    public function initRedis():RedisObject
    {
        $redis = PoolManager::getInstance()->getPool(RedisPool::class)->getObj(Config::getInstance()->getConf('REDIS.POOL_TIME_OUT'));
//        var_dump(PoolManager::getInstance()->getPool(RedisPool::class));
        if ($redis) {
            $this->redis = $redis;
        } else {
            //直接抛给异常处理，不往下
            throw new \Exception('error,redis Pool is Empty');
        }
        return $redis;
    }


    public function gcMysql()
    {
        // 请注意判断类型 避免将不属于该链接池的对象回收到池中
        if ($this->db instanceof MysqlObject) {
            PoolManager::getInstance()->getPool(MysqlPool::class)->recycleObj($this->db);

            // 请注意 此处db是该链接对象的引用 即使操作了回收 仍然能访问
            // 安全起见 请一定记得设置为null 避免再次被该控制器使用导致不可预知的问题
            $this->db = null;
        }
        return $this->db;
    }


    public function gcRedis()
    {
        if (!$this->redis instanceof RedisObject){
            var_dump($this->redis);
            echo "redis对象错误";
        }
        $flag = PoolManager::getInstance()->getPool(RedisPool::class)->recycleObj($this->redis);
        if ($flag){
            $this->redis = null;
            echo "回收redis 成功".PHP_EOL;
            return $this->redis;
        }else{
            echo "回收redis 失败";
        }
    }


    public function createData(string $table,array $data):array
    {
        $tableExists = $this->db->tableExists($table);
        if (!$tableExists){
            return [];
        }
        $sql = "SELECT 
           TABLE_SCHEMA AS `databaseName`,
           TABLE_NAME AS `tableName`,
           COLUMN_NAME AS `columnName`,
           DATA_TYPE AS `columnType`,
           COLUMN_COMMENT AS `columnComment` 
         FROM
           `information_schema`.`COLUMNS` 
         WHERE `TABLE_SCHEMA` = 'cmc' 
           AND `TABLE_NAME` = '{$table}' ;";
        $column =  $this->db->rawQuery($sql);
        $column = array_column($column,'columnName');
        if (empty($column)||empty($data)){
            return [];
        }
        foreach ($data as $k=>$v){
            if (!in_array($k,$column)){
                unset($data[$k]);
            }
        }
        return $data;
    }

    public function getInsertId($table)
    {
        $sql = "select AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'cmc' AND TABLE_NAME = '{$table}';";
        return $this->db->rawQuery($sql)[0]['AUTO_INCREMENT'];
    }


    public function response($result = [],$ret = 200,$msg='操作成功' ):array
    {
       $res = ResponseController::response($result,$ret,$msg);
       return $res;
    }

    public function remember($key_info,\Closure $fn,$flush = false)
    {
        if (!$key_info['key'] || !$key_info['time'] || strpos($key_info['key'],'*') !== false)
            throw new \Exception("CacheKey wrong,that parameters should not contain '*' ");


        $real = function () use ($key_info,$fn)
        {
            $data = $fn();
            $this->redis->set($key_info['key'],json_encode($data),$key_info['time']);

            return $data;
        };

        $data = $this->redis->get($key_info['key']);
        if ($flush === false && !empty($data))
            return json_decode($data,true);
        else
            return $real();

    }

    /**
     * 判断字符串是否为 Json 格式
     * @param string $data
     * @return bool
     */
    function isJson($data = '') {
        $data = json_decode($data);
        if ($data && (is_object($data)) || (is_array($data) && !empty(current($data)))) {
            return true;
        }
        return false;
    }


    public function __destruct()
    {
//        var_dump("销毁".get_class($this));
        // TODO: Implement __destruct() method.
        $this->gcRedis();
        $this->gcMysql();
        return true;
    }

}