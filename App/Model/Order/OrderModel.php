<?php


namespace App\Model\Order;


use App\Model\BaseAutoModel;

class OrderModel extends BaseAutoModel
{

    protected  $table = "wx_orders";

    public function orderStatusComplete()
    {

    }


    public function modifyOrderStatusByMac(string $mac,int $pay_status=2,int $shipping_status=0,int $shipping_status_modify=2)
    {
        $flag = $this->db
            ->where('mac',$mac)
            ->where('pay_status',$pay_status)
            ->where('shipping_status',$shipping_status)
            ->update($this->table,['shipping_status'=>$shipping_status_modify]);
        return $flag;
    }

}