<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/19
 * Time: 15:21
 * Auth: YWH
 */

namespace App\Model\User;


use App\Model\BaseAutoModel;


class WebsocketFdModel extends BaseAutoModel
{
    protected $FindFdByUid = "FindFdByUid";
    protected $FindUidByFd = "FindUidByFd";
    public function setFdAndUidMap($uid,$fd)
    {
        $this->redis->multi();
        #根据uid查找uid
        $this->redis->hSet('FindFdByUid',$uid,$fd);
        $this->redis->hSet('FindUidByFd',$fd,$uid);
        $this->redis->exec();
    }

    public function getFdByUid(int $uid)
    {
        $fd = $this->redis->hGet($this->FindFdByUid,$uid);
        return $fd;
    }



}