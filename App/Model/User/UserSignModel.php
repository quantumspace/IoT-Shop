<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/15
 * Time: 10:40
 * Auth: YWH
 */

namespace App\Model\User;

use App\Model\BaseAutoModel;
use App\Model\System\SystemConfModel;
use App\Serve\CacheKeyController;
use Co\Mysql\Exception;

class UserSignModel extends BaseAutoModel
{
    /**
     * 用户签到
     * @param int $uid
     * @return array|bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function sign(int $uid)
   {
       $confModel   = new SystemConfModel();
       $conf        = $confModel->getSystemConf();
       if (empty($conf['user_sign_exp'])){
           return self::response([],-1,'系统配置user_sign_exp错误');
       }
       $data_log    = [
           'uid'                   => $uid,
           'ip_id'                 => 0,
           'inc_val'               => $conf['user_sign_exp'],
           'inc_val_type'          => 1,
           'ip_log_time'           => time(),
           'ip_log_type'           => 2
       ];

       #开始事务
       $this->db->startTransaction();
       try{
           $user_exp_inc   = $this->db->where('id',$uid,'=')->setInc('cmc_user','user_exp',$conf['user_sign_exp']);
           $insert_log     = $this->db->insert('cmc_action_log',$data_log);
           if ($user_exp_inc&&$insert_log){
               $this->db->commit();
               $this->updateSignDataAtRedis($uid);
               return true;
           }else{
               $this->db->rollback();
           }
       }catch (Exception $exception){
           $this->db->rollback();
       }
       return false;
   }

   public function updateSignDataAtRedis($uid)
   {
       $offset     = $this->getOffset($uid);
       $keyInfo    = CacheKeyController::USER_INFO($uid);
       $flag       = $this->redis->setBit($keyInfo['key'],floor($offset),1);
       return $flag;
   }


   public function checkUserSignAtSomeTime($uid,$time=0)
   {
       if (empty($time)){
           $time        = time();
       }
       $keyInfo    = CacheKeyController::USER_INFO($uid);
       $offset     = $this->getOffset($uid,$time);
       $info       = $this->redis->getBit($keyInfo['key'],$offset);
       return $info;
   }

   public function checkIsSignFromMysql(int $uid)
   {
       $user_sign_first = $this->db
           ->where('uid',$uid,'=')
           ->where('log_type',2,'=')
           ->groupBy('log_id','asc')
           ->getOne("cmc_action_log");
       $sign_time_last  = $user_sign_first['log_time']??0;
       $offset      = (time() - $sign_time_last)/86400;
       if ($offset<1){
          return true;
       }else{
          return false;
       }
   }

   public function getOffset($uid,$time=0,$log_type = 2)
   {
       if (empty($time)){
           $time        = time();
       }
       #获取第一次签到的时间
       $user_sign_first = $this->db
           ->where('uid',$uid,'=')
           ->where('log_type',$log_type,'=')
           ->groupBy('log_id','asc')
           ->getOne("cmc_action_log");
       #本次是第一次签到
       if (empty($user_sign_first)){
           $offset      = 0;
       }else{
           $offset      = ($time - $user_sign_first['log_time'])/86400;
       }
       return $offset;
   }


   public function getUserSignInfoFromRedis($uid)
   {
       $keyInfo        = CacheKeyController::USER_INFO($uid);
       $userSignInfo   = $this->redis->get($keyInfo['key']);
       var_dump($userSignInfo);
   }
}