<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/10
 * Time: 14:31
 * Auth: YWH
 */

namespace App\Model\User;
use App\Model\BaseAutoModel;
use App\Serve\CacheKeyController;
use App\Serve\smsServer;
use function foo\func;

class SmsModel extends BaseAutoModel
{
    public function smsCodePersistence($data)
    {
        $result     = get_object_vars($data['result']);
        $taskData   = $data['taskData'];
        if (strtolower($result['Code']) == 'ok'){
            $key    = CacheKeyController::SMS_CODE_INFO($taskData['login_name']);
            $this->redis->setEx($key['key'],$key['time'],$taskData['code']);
            return true;
        }else{
            return false;
        }
    }

}