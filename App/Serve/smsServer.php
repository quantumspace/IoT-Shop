<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/10
 * Time: 9:48
 * Auth: YWH
 */

namespace App\Serve;
use  SmsDemo;

class smsServer
{
    public static function sendSms($phone,$templateParam,$tempId='SMS_129800010',$sign='次元局')
    {
        $response = SmsDemo::sendSms($sign,$tempId,$phone,$templateParam);
        $response = empty($response)?new \stdClass():$response;
        return $response;
    }
}