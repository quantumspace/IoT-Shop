<?php

namespace App\Serve;

class CacheKeyController
{
//    调用例子
//    $key = CacheKeyController::USER_INFO(2);
    /**
     * @param $name
     * @param $arguments
     * @返回 bool
     */
    public static function __callStatic($name,$arguments)
    {
        if(empty($arguments))
            return false;

        if (self::${$name})
        {
            $return = self::${$name};
            foreach ($arguments as $key => $value)
            {
                $return['key'] = preg_replace('/\*/', $value, $return['key'], 1);
            }
            return $return;
        }
        else
        {
            return false;
        }
    }

     # 系统配置
     public static $SYS_CONF    = ['key'=>'sys_conf','time'=>30];

     #  用户连续签到情况
     public static $USER_SIGN   = ['key'=>'user_sign|uid:*'];
        /**
         * 赛事下的ip排名,缓存10分钟
         */
      public  static $MATCH_RANK = ['key' => 'ip_*_rank','time' => 600];
        /**
         * ip报名信息,缓存100000分钟
         */
      public static $IP_ENROLL = ['key' => 'ip_*_enroll' , "time" => 6000000];
      #排名 旧的
      public  static $IP_VOTES_RANK = ['key'=>'ip_votes_rank'];
      #排名 最新
      public  static $IP_VOTES_RANK_OLD = ['key'=>'ip_votes_rank_old','3600'];


      public static $BANNER_LIST_STRING = ['key'=>'banner_list_string','time'=>60];
      #用户缓存 根据uid区分
      public static $USER_INFO         = ['key'=>'userInfoHash','time'=>3600];
      #Token缓存
      public static $TOKEN_INFO        = ['key'=>'TokenHash','time'=>3600];
      #验证码缓存
        /**@example $redis->setEx('SMSCodeHash|login_name:{$login_name}'，$value)**/
      public static $SMS_CODE_INFO     = ['key'=>'SMSCodeHash|login_name:*','time'=>1800];

        /***ip下面的粉丝列表**/
      public static $IP_USER_RELATION = ['key'=>'ip_user_relation|ip:*','time'=>86400];

    /**
     * ip列表缓存,分赛区,一天
     */
     public static $IP_LIST = ['key'=>'iplist_*','time'=>86400];
    /**
     * ip总列表,不分赛区
     */
    public static $ALL_IP = ['key'=>'all_ip_*','time'=>86400];
    public static $IP_SIGN_SET = ['key'=>'ip_sign_list|ip_id:*'];
    public static $IP_WEIBO = ['key'=>'ip_weibo_*','time'=>86400];
    public static $USER_TOKEN = ['key'=>'user_token|uid:*','time'=>30*86400];
    /**
     * ip的粉丝数量
     */
    public static $IP_FANS_NUM = ['key' => "ip_fans_*",'time'=>86400];
    /**
     * 平台ip的数量
     * @var array
     */
    public static $IP_NUM = ['key' => "ip_num",'time'=>86400];
    /**
     * 粉丝助力榜
     * @var array
     */
    public static $FANS_RANK = ['key' => "exp_rank|ip:*,datemin:*,datemax:*,page:*",'time'=>86400];
    /**
     * 粉丝经验值排行,缓存10分钟
     * @var array
     */
    public static $EXP_RANK = ['key' => "exp_rank|date:*,new:*",'time'=>10*60];

    /**
     * rank_list表缓存
     * @var array
     */
    public static $RANK_LIST_HISTORY = ['key' => "rank_list_history|type:*,date:*,match:*",'time' => 86400];
    /**
     * 榜单播报缓存
     * @var array
     */
    public static $RANK_LIST_USER = ['key' => "rank_list_user|uid:*,date:*",'time' => 86400];
    /**
     * 可选日期列表
     * @var array
     */
    public static $RANK_LIST_DATE = ['key' => "rank_list_date|match_id:*,type:*",'time' => 86400];


}